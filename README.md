HaXePunk GUI
============

##1. Introduction
HaXePunk GUI is a toolset to display user interfaces that works with HaXePunk engine. It provide some basic components to allow the user to interact with the game.
[Try the demo](http://samuel-bouchet.fr/haxepunk-gui/)

##2. Key Features
- **Standards components** : Button, ToggleButton, CheckBox, RadioButton, Label, TextArea, Panel, Window
- **Advanced components** : Carousel, MenuList, MenuItem
- **Hierarchical composition** of components with relative positionning
- **Event support** (clicked, resized, hidden, shown, added\_to\_container, added\_to\_world, removed\_from\_container, removed\_from\_world, mouse\_hover, mouse\_out, etc. )
- **Customizable skins** (by editing skin PNG file)

##3. How to use
###3.0. Installation
``haxelib install HaxePunk-gui`` or get it from the the [haxelib project page](http://lib.haxe.org/p/HaxePunk-gui).

You will need to add some assets (currently 4 skins and 1 new font) to your haxepunk project. They are available in the package at [haxelib project page](http://lib.haxe.org/p/HaxePunk-gui) as well, or directly [here](http://samuel-bouchet.fr/haxepunk-gui/assets.zip).

###3.1. The ONE thing to remember
There is ONE thing to remember when using (or creating) HaXePunk GUI components : 
>ALWAYS use localX and localY attributes to place/move a component.

This is required to allow hierarchical imbrication of components since there are no accessors to x and y attributes that can be overriden.
The component or its container will be displayed in the world at the right place automatically, do not try to write x and y attributes instead.
If you need to know the component absolute position in the world, you can use the absoluteX and absoluteY read-only attributes, or read x and y without modifying them.

###3.2. How it works
To use those components, you may want to understand how they work in HaXePunk Context.

- Every component inherits com.haxepunk.gui.Control. com.haxepunk.gui.Control itself inherits com.haxepunk.Entity. To use a component, instantiate it and place it in your world.
- Every Control can add child Controls. Child position become relative to his container's.
- A child absolute position is updated by its container using its localX and localY attributes.
- Components reacts automatically to user interaction by firing events (of type com.haxepunk.gui.event.ControlEvent). Events must be catch the flash way, i.e. : myComponent.addEventListener(Control.RESIZED, eventCatcherFunction);

###3.3. Initialization
First, you may want to tweak some apects of the components. Here is an list of everything you can customize before starting using components :
    
	:::actionscript
	// Choose custom skin. Parameter can be a String to resource or a bitmapData.
	Control.useSkin("gfx/ui/defaultGuiSkin2.png");
	
	// The default layer where every component will be displayed on.
	// Most components use severals layers (at least 1 per component child).              |
	// A child component layer will be <100.
	Control.defaultLayer = 100;
	
	// Padding between border and inner text. Set the default padding of every new Button 
	// and ToggleButton. The padding attribute can be changed on instances after creation.
	Button.defaultPadding = 4;
	
	// Size in px of the tickBox for CheckBoxes. Default is skin native size : 12.
	CheckBox.defaultBoxSize = 12;
	
	// Same for RadioButtons.
	RadioButton.defaultBoxSize = 12;
	
	// Label defaults parameters affect every components that uses labels :
	// Button, ToggleButton, CheckBox, RadioButton, MenuItem, Window Title.
	// Those labels can still be edited any time using "myComponent.label" attribute.
	
	// Label default font (must be a nme.text.Font object).
	Label.defaultFont = nme.Assets.getFont("font/pf_ronda_seven.ttf");
	// Label defaultColor. Tip inFlashDevelop : use ctrl + shift + k to pick a color.
	Label.defaultColor = 0x1E4E82;
	// Label default Size.
	Label.defaultSize = 8;

###3.4. Basic Components (exemples to be added)
####3.4.0. General behaviour
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Control.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Control.html)
####3.4.1. Label
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Label.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Label.html)
####3.4.2. Button and ToggleButton
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Button.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Button.html)

See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/ToggleButton.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/ToggleButton.html)
####3.4.3. CheckBox
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/CheckBox.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/CheckBox.html)
####3.4.4. RadioButton
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/RadioButton.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/RadioButton.html)
####3.4.5. TextInput
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/TextInput.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/TextInput.html)
####3.4.6. Panel
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Panel.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Panel.html)
####3.4.7. Window
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Window.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Window.html)
###3.5. Advanced Components
####3.5.1. Carousel
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Carousel.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/Carousel.html)
####3.5.2. MenuList and MenuItem
See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/MenuList.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/MenuList.html)

See [http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/MenuItem.html](http://samuel-bouchet.fr/haxepunk-gui/API/content/com/haxepunk/gui/MenuItem.html)

##4. Versions
###(current) 2.5.2 Changes - Requires HaxePunk 2.5.2 (+ openfl 1.2.3) - 25 Mar 2014
- fix issue with close button on Window (Thanks to Santiago Vilar)
- fix crash on native target with TextInput (Thanks to Gabriel Rosauro)
- fix TextInput behaviour when enabled/disabled
- optimize NineSlice performance on flash target (Thanks to Gabriel Rosauro)

###(current) 2.5.0 Changes - Requires HaxePunk 2.5.0 (+ openfl 1.2.2) - 08 Fev 2014
- update for haxepunk 2.5.0

### 2.4.6 Changes - Requires HaxePunk 2.4.5/2.4.6 (+ openfl 1.2.2) - 18 Jan 2013
- Now run correctly with native targets (Yay !)
- Fix issue with windows dragging
- Fix issue with button being still active in a non visible scene

### 2.4.5 Changes - Requires HaxePunk 2.4.5/2.4.6 (+ openfl 1.2.2) - 04 Jan 2013
- updated for HaxePunk 2.4.5
- Fix conflit with Entity "followCamera" property. Renammed "stickToCamera" in haxepunk-gui Control.

### 2.4 Changes - Requires HaxePunk 2.4.1 (on nme 3.5.3) - 21 Oct 2013
- updated for HaxePunk 2.4.1
- Fix MenuItem size issue
- (Demo) Fix Panel size problem at init


### 2.0 Changes - Requires HaxePunk 2.0.2 (on nme 3.5.3) - 26 Jan 2013
- updated for HaxePunk 2.0.2 (thanks Matt!)
- New ``radioButton.select()`` method
- New ``control.followCamera`` Bool attribute. Fix the control using screen coordinate if followCamera is true.
- Fix issue where a component would stay displayed is addControl and removeControl are called during the same update for a same instance.
- Events MOUSE_OUT and MOUSE_HOVER now exists for every controls
- Fix placement issues when camera is moving
- Fix dragging issues of child windows when its container is moving
- Fix button label size problem at creation
- Fix events not removed from removed Controls

### 1.1 Changes - Requires HaxePunk 1.7.0 - 27 Nov 2013
- Control.removeChild now removes correctly the child component from the world
- Fixed Carousel layering issues
- Only topmost Control get mouse stimulations
- Add Control.contains function to know if a control is already a child.
- Control.addChild won't add a Control if it is already a child.
- Carousel mouse drag to roll can be disabled : ``carousel.allowDragToRoll = false``
- RadioButton can now change group after initialization
- Fixed Button label alignment issues with fixed width
- Fixed Button hitbox issue when resizing label
- Performance optimizations (reduce needed memory to display Buttons)
- Fixed a visual glitch occurring when some Controls appears for the first time

### 1.0 - Requires HaxePunk 1.6.7 - 04 Nov 2013
Changes :
- initial release.